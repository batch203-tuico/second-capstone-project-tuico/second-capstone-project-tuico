const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");
// ============================================================


// CHECKING IF EMAIL EXISTS
// module.exports.checkEmailExists = (req, res) => {
//     return User.find({ email: req.body.email }).then(result => {

//         // The result of the find() method returns an array of objects.
//         // we can use array.length method for checking the current result length
//         console.log(result);

//         // The user already exists
//         if (result.length > 0) {
//             // return res.send(true);
//             return res.send("User already exists!");

//             // There is no duplicate found.
//         } else {
//             // return res.send(false);
//             return res.send("No duplicate found! Proceed to registration.")
//         }
//     })
//         .catch(error => res.send(error))
// };
// ============================================================

// USER REGISTRATION
module.exports.registerUser = (req, res) => {
    // const userData = auth.decode(req.headers.authorization);

    let newUser = new User({
        firstName: req.body.firstName,
        lastName:req.body.lastName,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
        mobileNo: req.body.mobileNo
    })

    // let newUser = new User({req.body});

    // console.log(newUser);

    return newUser.save()
    .then(user => {
        // console.log(user);
        res.send(true);
        // res.status(201).send("User registration is successful!"); //notification
    })
    .catch(error => {
        // console.log(error);
        res.send(error);
        // res.status(400).send("Registration failed. User already exists!");
    })
};
// =============================================================


// LOGIN (USER AUTHENTICATION)
module.exports.loginUser = (req, res) => {
    return User.findOne({email: req.body.email})
    .then(result => {
        
        // if user does not exist
        if (result == null){
            return res.send(false);
            // return res.send({message: "No user found."});

        // if user exist
        } else {
            // decrypting and comparing the password
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

            // if password matches
            if (isPasswordCorrect) {
                return res.send({accessToken: auth.createAccessToken(result)});
                // return res.send(true);

            // if password is incorrect
            } else {
                // return res.status(401).send({message: "Incorrect password!"})
                return res.send(false);
            }
        }
    })
};
// =============================================================

// GET ALL USER
module.exports.retrieveAllUsers = (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
        User.find({})
        // .then(result => res.send(result))
        .then(result => res.send(true))
        .catch(error => {
        console.log(error)
        res.send(error)
        })
    } else {
        // return res.status(401).send("You don't have access to this page!");
        return res.send(false);
    }
    
};
// =============================================================


// RETRIEVE USER DETAILS
module.exports.getProfile = (req, res) => {

        // Payload of JWT
        const userData = auth.decode(req.headers.authorization);

        console.log(userData);

    if (userData.isAdmin) {
        return User.findById(userData.id)
        .then(result => {
            result.password = ""; // -************
            // res.status(200).send(result);
            res.status(200).send(true);
        })
        .catch(error => {
            console.log(error);
            // res.status(401).send(error);
            res.status(401).send(error);
        })
    } else {
        return User.findById(userData.id,
        {
            firstName: 1,
            lastName: 1,
            email: 1,
            mobileNo: 1,
            orders: 1,
            _id: 0
        })
        .then(result => {
          
            console.log(result);
            // res.status(200).send(result);
            res.status(200).send(true);
        })
        // .then(result => res.status(200).send(result))
        .catch(error => {
            console.log(error);
            res.status(401).send(error);
        })
    }
};
// =============================================================

// CREATE AN ORDER (USER BUY PRODUCT)
module.exports.createOrder = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin == false) {
        
        let isUserUpdated = await User.findById(userData.id)
        .then(user => {
            console.log(user)

            user.orders.push({
                totalAmount: req.body.totalAmount,
                products: req.body.products
            })
        return user.save()
        .then(result => {
            console.log(result);
            // res.send(result);
            res.send(true);
        })
        .catch(error => {
            console.log(error);
            res.send(error);
        })
        })
    
        // to loop the products ordered (depends on how many products the user ordered)
        for (let i=0; i < req.body.products.length; i++){

        let data = {
            userId: userData.id,
            email: userData.email,
            productId: req.body.products[i].productId,
            productName: req.body.products[i].productName,
            quantity: req.body.products[i].quantity
        }

        let isProductUpdated = await Product.findById(data.productId).then(product => {
        product.orders.push({
            userId: data.userId,
            email: data.email,
            quantity: req.body.quantity
        })

        // Minus the stocks available per order
        product.stocks -= data.quantity;

        return product.save()
        .then(result => {
            console.log(result);
            res.send(true);
        })
        .catch(error => {
            console.log(error);
            res.send(error);
        })
        })    
     }
    } else {
        // return res.status(401).send("You cannot order products as admin.");
        return res.send(false);
    }
};
// ============================================================

// STRETCH GOALS

// SET USER AS ADMIN (ADMIN ONLY)
module.exports.setUserToAdmin = (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    const makeUserAdmin = {
        isAdmin: req.body.isAdmin
    };

    if (userData.isAdmin) {
        return User.findByIdAndUpdate(req.params.userId, makeUserAdmin, {new: true})

        .then(result => {
            console.log(result);
            // res.status(200).send(result);
            res.status(200).send(true);
        })
        .catch(error => {
            console.log(error);
            res.status(401).send(error);
        })

    } else {
        // return res.status(401).send("You don't have access to this page!");
        return res.send(false);
    }
};
// ======================================================================

// RETRIEVE ALL ORDERS (FOR ADMIN ONLY)
// module.exports.getAllOrders = (req, res) => {
//     const userData = auth.decode(req.headers.authorization);

//     // let orders = Product.orders;

//     if (userData.isAdmin) {
//         return Product.find({})
//         .then(result => res.status(200).send(result))
//         .catch(error => {
//             console.log(error)
//             res.status(false)
//         })
//     } else {
//         return res.status(401).send("You don't have access to this page!")
//     }
// };
