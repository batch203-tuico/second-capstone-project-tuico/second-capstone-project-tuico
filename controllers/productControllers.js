const Product = require("../models/Product");
const auth = require("../auth");
// ===================================================================


// PRODUCT CREATION (FOR ADMIN ONLY)
module.exports.createProduct = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    console.log(userData);


    // Variable to contain the product
    let newProduct = new Product({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        stocks: req.body.stocks
    });

    // to check the values in the req.body being passed
    console.log(newProduct);

    if (userData.isAdmin) {
        newProduct.save()

        // Product is created
        .then(product => {
            console.log(product);
            // res.status(201).send("Product successfully created!")
            res.status(201).send(true);
        })

        // if there is an error
        .catch(error => {
            console.log(error);
            res.status(400).send(error);
        })
    } else {
        // return res.status(401).send("You cannot create a product. For admin users only!");
        res.send(false);
    }
};
// ===================================================================

// RETRIEVE ALL ACTIVE PRODUCTS (BOTH ADMIN, CUSTOMERS, AND GUESTS)
module.exports.retrieveAllActiveProducts = (req, res) => {
    
         return Product.find({isActive: true},
            {
                name: 1,
                description: 1,
                price: 1,
                stocks: 1,
                _id: 0

            })
        
        .then(result => {
            // to check values in terminal
            console.log(result);
            // to display result in postman
            // res.status(200).send(result);
            res.status(200).send(true);
        })
        .catch(error => {
            console.log(error);
            res.status(401).send(error);
        })
};
// ===================================================================

// RETRIEVE ALL PRODUCTS (FOR ADMIN ONLY)
module.exports.retrieveAllProducts = (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
        return Product.find({})
        .then(result => {
            console.log(result);
            // res.status(200).send(result)
            res.status(200).send(true);
        })
        .catch(error => {
            console.log(error);
            res.status(401).send(error);
        })
    // If not admin
    } else {
        // return res.status(401).send("You don't have access to this page!");
        return res.send(false);
    }
};
// ===================================================================

// RETRIEVE A SPECIFIC PRODUCT (ADMIN AND USER)
module.exports.getProduct = (req, res) => {

        return Product.findById(req.params.productId,
        {
            name: 1,
            description: 1,
            price: 1,
            stocks: 1,
            orders: 1,
            _id: 0
        })
        .then(result => {
            console.log(result);
            // res.status(200).send(result);
            res.status(200).send(true);
        })
        .catch(error => {
            console.log(error);
            // res.status(401).send(error);
            res.status(401).send(error);
        })
};
// ===================================================================

// UPDATING PRODUCT INFORMATION (ADMIN ONLY)
module.exports.updateProduct = (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
        const updatedProduct = {
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
            stocks: req.body.stocks
        };

        // to check values being passed in the body
        console.log(updatedProduct);

        return Product.findByIdAndUpdate(req.params.productId, updatedProduct, {new: true})

        .then(result => {
            console.log(result);
            // res.status(200).send(result);
            res.status(200).send(true);
        })
        .catch(error => {
            console.log(error);
            res.status(401).send(error);
        })
    } else {
        //  return res.status(401).send("You don't have access to this page!");
        return res.send(false);
    }
};
// ===================================================================

// ARCHIVE A PRODUCT (ADMIN ONLY)
module.exports.archiveProduct = (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    const updateIsActive = {
        isActive: req.body.isActive
    };

    if (userData.isAdmin) {
        return Product.findByIdAndUpdate(req.params.productId, updateIsActive, {new: true})

        .then(result => {
            console.log(result);
            // res.status(200).send(result);
            res.status(200).send(true);
        })
        .catch(error => {
            console.log(error);
            res.status(401).send(error);
        })
    } else {
        // return res.status(401).send("You don't have access to this page!");
        return res.send(false);
    }
};
// ===================================================================