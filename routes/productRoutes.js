const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

console.log(productControllers);

// PRODUCT CREATION ROUTE (FOR ADMIN ONLY)
router.post("/", auth.verify, productControllers.createProduct);

// RETRIEVE ALL ACTIVE PRODUCTS (ADMIN, USER, GUEST)
router.get("/", productControllers.retrieveAllActiveProducts);

// RETRIEVE ALL PRODUCTS (FOR ADMIN ONLY)
router.get("/all", auth.verify, productControllers.retrieveAllProducts);

// RETRIEVE A PRODUCT
router.get("/:productId", productControllers.getProduct);

// UPDATE A PRODUCT (ADMIN ONLY)
router.put("/:productId", auth.verify, productControllers.updateProduct);

// ARCHIVE A PRODUCT
router.patch("/:productId", auth.verify, productControllers.archiveProduct);


















module.exports = router;