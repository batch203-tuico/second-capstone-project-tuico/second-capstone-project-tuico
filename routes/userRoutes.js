const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

console.log(userControllers);
// ============================================================

// CHECK IF EMAIL EXISTS
// router.post("/checkEmail", userControllers.checkEmailExists);

// USER REGISTRATION
router.post("/register", userControllers.registerUser);

// RETRIEVE ALL USERS
router.get("/", userControllers.retrieveAllUsers);

// USER LOGIN (AUTHENTICATION)
router.post("/login", userControllers.loginUser);

// GET A PROFILE
router.get("/details", auth.verify, userControllers.getProfile);

// USER BUY A PRODUCT
router.post("/checkout", auth.verify, userControllers.createOrder);

// SET USER AS ADMIN (ADMIN ONLY)
router.patch("/:userId", auth.verify, userControllers.setUserToAdmin);

// GET ALL ORDERS (FOR ADMIN ONLY)
// router.get("/orders", auth.verify, userControllers.getAllOrders);












// ============================================================
module.exports = router;