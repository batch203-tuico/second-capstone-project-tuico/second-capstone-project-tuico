const jwt = require("jsonwebtoken");
const User = require("./models/User");

// secret is for signature validation
const secret = "EcommerceAPI";
// ==========================================================



// TOKEN CREATION
module.exports.createAccessToken = (user) => {
    console.log(user);

    // payload of the JWT
    const data = {
        id: user._id,
        username: user.username,
        email: user.email,
        isAdmin: user.isAdmin
    };

    return jwt.sign(data, secret, {});
};
// ==========================================================



// ACT AS MIDDLEWARE FUNCTION
module.exports.verify = (req, res, next) => {

    // The token is retrieved from the request header.
    let token = req.headers.authorization;

    // console.log(token);

    if (token !== undefined) {

        // The token sent is a type of "Bearer" which when received contains the "Bearer" as a prefix to the string. To remove the "Bearer" prefix we used the slice method to retrieve only the token.
        token = token.slice(7, token.length);

        console.log(token);

        // Validate the "token" using the "verify" method to decrypt the token using the secret code.
        // Syntax: jwt.verify(token, secretOrPrivateKey, [options/callbBackFunctions])

        return jwt.verify(token, secret, (error, data) => {
            // if jwt is not valid
            if (error) {
                return res.send({authentication: "Invalid token!"});
            
            // if jwt is valid
            } else {
                // Allows the application to proceed with the next middleware function/callback function to the route.
                next();
            }
        })

    } else {
        res.send({error: "Authentication failed. No token provided!"});
    }
};
// ==========================================================


// TOKEN DECRYPTION
module.exports.decode = (token) => {
    if (token !== undefined) {
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (error, data) => {
            if (error) {
                // if token is not valid
                return null;
            } else {
                // decode method is used to obtain the information from the JWT
                // Syntax: jwt.decode(token, [options]);
                // Returns an object with access to the "payload" property which contains the user information stored when the token is generated.
                return jwt.decode(token, {complete: true}).payload;
            }
        })
    } else {
        // if token does not exist
        return null;
    }
};
// ==========================================================




