// PACKAGES
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
// const bcrypt = require("bcrypt");
// ===================================================



// PORT
const port = process.env.PORT || 4000;
// ===================================================



// ROUTES DECLARATION
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
// ===================================================



// MIDDLEWARES
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
// ===================================================



// MONGODB DATABASE
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.7jqbank.mongodb.net/ecommerce_API?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
)
    .then(() => console.log(`We're connected to our cloud database`))
    .catch((error) => {
        console.log(error)
    });
// ===================================================



// ROUTES GROUPING
// USER ROUTES
app.use("/api/users", userRoutes);

// PRODUCT ROUTES
app.use("/api/products", productRoutes);
// ===================================================








// PORT LISTENER
app.listen(port, () => {
    console.log(`API is now online on port ${port}`)
});
// ===================================================